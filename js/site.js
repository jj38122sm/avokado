const navToggle = document.querySelector('.nav__toggle');

// Add/remove .mobile-on CSS class.
navToggle.addEventListener('click', function() {
    this.nextElementSibling.classList.toggle('mobile-on');
});

// Remove .mobile-on CSS class if window is resized above 992px.
window.addEventListener('resize', function() {
    var ww = window.innerWidth;
    if (992 <= ww) {
        navToggle.nextElementSibling.classList.remove('mobile-on');
    }
});

// Init masonry on page load.
window.onload = function() {
    const container = document.querySelector('.grid');
    var masonry = new Masonry(container, {
        itemSelector: '.grid__item',
        columnWidth: 124,
        gutter: 18
    });

};
