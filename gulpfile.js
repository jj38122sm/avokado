/*------------------------------------------------------------------------------
# Gulp Config
------------------------------------------------------------------------------*/

var URL = 'http://localhost/avokado';

/*------------------------------------------------------------------------------
# Gulp Plugins
------------------------------------------------------------------------------*/

var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin       = require('gulp-cssmin'),
    imagemin     = require('gulp-imagemin'),
    rename       = require('gulp-rename'),
    sass         = require('gulp-sass'),
    uglify       = require('gulp-uglify'),
    pngquant     = require('imagemin-pngquant'),
    gcmq         = require('gulp-group-css-media-queries');

/*------------------------------------------------------------------------------
# AUTOPREFIXER
------------------------------------------------------------------------------*/

// Browsers you care about for autoprefixing.
// Browserlist https://github.com/ai/browserslist
const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
];

/*------------------------------------------------------------------------------
# Gulp Tasks
------------------------------------------------------------------------------*/

gulp.task('scripts', function () {
    return gulp.src(['assets/js/**/**.js', '!assets/js/**/**.min.js'])
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('assets/js/'))
});

gulp.task('imagemin', function () {
    return gulp.src('images/**')
        .pipe(
            imagemin({
                progressive: true,
                interlaced: true,
                svgoPlugins: [{
                        removeViewBox: false
                    },
                    {
                        cleanupIDs: false
                    }
                ],
                use: [pngquant()]
            })
        )
        .pipe(gulp.dest('images'))
});

gulp.task('sass', function () {
    return gulp.src('scss/**/**.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            Browserlist: AUTOPREFIXER_BROWSERS,
        }))
        .pipe(gcmq())
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
});

gulp.task('serve', function () {
    browserSync.init({
        proxy: URL,
        notify: false,
        injectChanges: true,
        ghostMode: {
            forms: false,
        }
    });

    gulp.watch("scss/**/**.scss", gulp.series( ['sass'] ));
    gulp.watch(["**/*.html", "**/*.js", "!node_modules/**"]).on('change', browserSync.reload);
});
